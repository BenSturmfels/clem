import os

from fabric import task
from invoke.collection import Collection

from vps_deploy import django_fabric2 as df2

hosts = ['deploy@calais.sturm.com.au']


@task(hosts=hosts)
def deploy(c):
    """Install or deploy updates this website."""

    if not c.config.sudo.password:
        c.config.sudo.password = df2.read_gpg_password_file(c, os.environ['GPG_ENCRYPTED_SUDO_PASSWORD_FILE'])
    df2.fix_permissions(
        c,
        read=['htdocs'],
        read_write=[])
    df2.transfer_files_git(c)
    df2.update_nginx(c)


# The "ns" appears to be a magic name.
ns = Collection(
    deploy,
    task(df2.download_postgres_db, hosts=hosts),
    task(df2.mirror_postgres_db, hosts=hosts),
    task(df2.mirror_media, hosts=hosts),
    task(df2.django_shell, hosts=hosts),
    task(df2.bash, hosts=hosts),
)
ns.configure({
    # Built-in Fabric config.
    'run': {
        'echo': True,
        # Needed so local commands work. Can also use FABRIC_RUN_REPLACE_ENV.
        'replace_env': False,
    },

    # Our custom project config.
    'env': {
        'branch': 'master',
        'app_user': 'www-data',
        'project_dir': '/srv/clem',
        'site_name': 'clem',
        'nginx_conf': 'deploy/nginx.conf',
        'url': 'http://clem.sturmfels.com.au/',
    },
})

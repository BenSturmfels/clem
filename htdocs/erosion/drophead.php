<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
<title>The Soil &amp; Land Portal</title>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" />
<meta name="MSSmartTagsPreventParsing" content="true" />

<meta name="author" content="Ben Sturmfels" />

<link rel="stylesheet" href="../simple.css" type="text/css" />
<style type="text/css" media="all">@import "../complex.css";</style>
<link rel="stylesheet" href="../print.css" type="text/css" media="print" />

<script type="text/javascript" src="../scripts/menu.js"></script>
</head>

<body>
<div class="hide"><a href="#content" title="Skip navigation." accesskey="2">Skip navigation</a>.</div>

<div id="header">
<?php 
$toroot = "..";
require("$toroot/scripts/header.php");
?>
</div>

<div id="wrapper">
<div id="nav">
<?php require("$toroot/scripts/menu.php"); ?>
</div>

<div id="content">
<h1>Concrete Head Structure</h1>

<div class="figleft"><img src="images/rcdrop.jpg" width="240" height="160" alt="rcdrop.jpg - 12019 Bytes" /><p style="width: 240px;">Drop</p></div>

<div class="figleft"><img src="images/rcchute.jpg" width="240" height="159" alt="rcchute.jpg - 10302 Bytes" /><p style="width: 240px;">Chute</p></div>

<h2 style="clear: left">Description</h2>
<p>These structures are built on site from reinforced concrete. They consist of a headwall and notch, a drop or chute section and a stilling pool to dissipate energy. Cost $20,000 plus.</p>
<h2>Application</h2>
<p>Used in larger catchments to control headward erosion where diversion and gully battering is not feasible.</p>
<h2>Limitations</h2>
<p>Not suitable for sites with an unstable gully floor. Unsuitable for sites with deep cracking clay, deep tunnels or sub-surface seepage. Prone to failure from inadequate foundations. Difficult to repair.</p>
<h2>Comments</h2>
<p>These structures are difficult to build and require specialised equipment and experienced construction crews.</p>

</div>
</div>

<div id="footer">
<?php require("$toroot/scripts/footer.php"); ?>
</div>

</body>
</html>

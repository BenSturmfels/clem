<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
<title>The Soil &amp; Land Portal</title>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" />
<meta name="MSSmartTagsPreventParsing" content="true" />

<meta name="author" content="Ben Sturmfels" />

<link rel="stylesheet" href="../simple.css" type="text/css" />
<style type="text/css" media="all">@import "../complex.css";</style>
<link rel="stylesheet" href="../print.css" type="text/css" media="print" />

<script type="text/javascript" src="../scripts/menu.js"></script>
</head>

<body>
<div class="hide"><a href="#content" title="Skip navigation." accesskey="2">Skip navigation</a>.</div>

<div id="header">
<?php 
$toroot = "..";
require("$toroot/scripts/header.php");
?>
</div>

<div id="wrapper">
<div id="nav">
<?php require("$toroot/scripts/menu.php"); ?>
</div>

<div id="content">
<h1>Gully Battering</h1>

<div class="figleft"><img src="images/gbbefore.jpg" width="240" height="162" alt="gbbefore.jpg - 14049 Bytes" /><p style="width: 240px;">Before</p></div>
<div class="figleft"><img src="images/gbafter.jpg" width="240" height="159" alt="gbafter.jpg - 11018 Bytes" />
<p style="width: 240px;">After</p></div>

<h2 style="clear: left">Description</h2>
<p>Gully battering involves filling the eroded channel with soil to form a grassed parabolic waterway. The process involves stripping back the topsoil, battering in the gully sides and respreading the topsoil over the finished work. Cost $3 - $10 per lineal metre.</p>
<h2>Application</h2>
<p>A common technique used in the Upper Wimmera Catchment to control active headward and gully floor erosion and to eradicate rabbits.</p>
<h2>Limitations</h2>
<p>Not suitable for sites with perennial saline flows.Disturbed areas must be protected from run-off using diversion banks or similar structure.Needs to be protected from grazing for at least one year.</p>
<h2>Comments</h2>
<p>Essential that all topsoil is removed and replaced over the finished work. In larger jobs an excavator is recommended for stripping topsoil. Irregular or steep sided gullies are usually battered using a bulldozer while large articulated graders are recommended for more straightforward jobs.</p>

</div>
</div>

<div id="footer">
<?php require("$toroot/scripts/footer.php"); ?>
</div>

</body>
</html>

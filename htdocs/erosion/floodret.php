<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
<title>The Soil &amp; Land Portal</title>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" />
<meta name="MSSmartTagsPreventParsing" content="true" />

<meta name="author" content="Ben Sturmfels" />

<link rel="stylesheet" href="../simple.css" type="text/css" />
<style type="text/css" media="all">@import "../complex.css";</style>
<link rel="stylesheet" href="../print.css" type="text/css" media="print" />

<script type="text/javascript" src="../scripts/menu.js"></script>
</head>

<body>
<div class="hide"><a href="#content" title="Skip navigation." accesskey="2">Skip navigation</a>.</div>

<div id="header">
<?php 
$toroot = "..";
require("$toroot/scripts/header.php");
?>
</div>

<div id="wrapper">
<div id="nav">
<?php require("$toroot/scripts/menu.php"); ?>
</div>

<div id="content">
<h1>Flood Retention Dam</h1>

<div class="figleft"><img src="images/floodbefore.jpg" width="240" height="160" alt="floodbefore.jpg - 9645 Bytes" /><p style="width: 240px;">Before</p></div>

<div class="figleft"><img src="images/floodafter.jpg" width="240" height="159" alt="floodafter.jpg - 7313 Bytes" />
<p style="width: 240px;">After</p></div>

<h2 style="clear: left">Description</h2>
<p>Large dam which stores floodwater for a short period during storm events. Water is released at a controlled rate through a low-level outlet pipe. Cost $2 - $3 per cubic metre of bank.</p>
<h2>Application</h2>
<p>These structures are used to control downstream flooding, to protect eroding gully heads and to facilitate the collection and disposal of water.</p>
<h2>Limitations</h2>
<p>High risk of failure in tunnel prone soils.</p>
<p>Must have adequate flood bypass facilities.</p>
<p>Requires flat wide valley to achieve high storage efficiency.</p>
<h2>Comments</h2>
<p>There are not many sites well suited to these structures due to poor water storage conditions.</p>
<p>Design is difficult due to the lack of detailed stream flow data.</p>

</div>
</div>

<div id="footer">
<?php require("$toroot/scripts/footer.php"); ?>
</div>

</body>
</html>

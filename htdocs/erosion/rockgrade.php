<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
<title>The Soil &amp; Land Portal</title>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" />
<meta name="MSSmartTagsPreventParsing" content="true" />

<meta name="author" content="Ben Sturmfels" />

<link rel="stylesheet" href="../simple.css" type="text/css" />
<style type="text/css" media="all">@import "../complex.css";</style>
<link rel="stylesheet" href="../print.css" type="text/css" media="print" />

<script type="text/javascript" src="../scripts/menu.js"></script>
</head>

<body>
<div class="hide"><a href="#content" title="Skip navigation." accesskey="2">Skip navigation</a>.</div>

<div id="header">
<?php 
$toroot = "..";
require("$toroot/scripts/header.php");
?>
</div>

<div id="wrapper">
<div id="nav">
<?php require("$toroot/scripts/menu.php"); ?>
</div>

<div id="content">
<h1>Grade Control Structure - Rock Chute</h1>

<div class="figleft"><img src="images/rockchtbefore.jpg" width="240" height="157" alt="rockchtbefore.jpg - 12759 Bytes" /><p style="width: 240px;">Before</p></div>

<div class="figleft"><img src="images/rockchtafter.jpg" width="240" height="160" alt="rockchtafter.jpg - 16760 Bytes" />
<p style="width: 240px;">After</p></div>

<h2 style="clear: left">Description</h2>
<p>Shallow layer of dense angular rock used to build a long sloping chute. Total fall usually limited to about 1m. The design incorporates an upstream notch, a chute section and an energy dissipating area. Cost $6,000 plus.</p>
<h2>Application</h2>
<p>Used to control active bed erosion in gullies, streams and rivers.</p>
<h2>Limitations</h2>
<p>Requires good supply of dense angular rock within reasonable cartage distance.</p>
<h2>Comments</h2>
<p>Filter cloth must be used in soils prone to slaking or dispersion. These structures are "flexible" easy to maintain and have an indefinite life.</p>

</div>
</div>

<div id="footer">
<?php require("$toroot/scripts/footer.php"); ?>
</div>

</body>
</html>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
<title>The Soil &amp; Land Portal</title>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" />
<meta name="MSSmartTagsPreventParsing" content="true" />

<meta name="author" content="Ben Sturmfels" />

<link rel="stylesheet" href="../simple.css" type="text/css" />
<style type="text/css" media="all">@import "../complex.css";</style>
<link rel="stylesheet" href="../print.css" type="text/css" media="print" />

<script type="text/javascript" src="../scripts/menu.js"></script>
</head>

<body>
<div class="hide"><a href="#content" title="Skip navigation." accesskey="2">Skip navigation</a>.</div>

<div id="header">
<?php 
$toroot = "..";
require("$toroot/scripts/header.php");
?>
</div>

<div id="wrapper">
<div id="nav">
<?php require("$toroot/scripts/menu.php"); ?>
</div>

<div id="content">
<h1>Diversion Banks</h1>

<div class="figleft"><img alt="Diversion bank before" src="images/dbbefore.jpg" width="240" height="160" /><p style="width: 240px;">Before</p></div>

<div class="figleft"><img alt="Diversion bank after" src="images/dbafter.jpg" width="240" height="160" /><p style="width: 240px;">After</p></div>

<h2>Description</h2>
<p>Earthern, grassed bank constructed on contour. Usually built with a bulldozer or grader. Grade of bank usually 0.3% to 0.7%. Bank constructed using clay from either above (cut &amp; fill) or below (all fill) the bank line. Cost $2 - $3 per lineal metre</p>
<h2>Application</h2>
<p>Used to transport water away from active soil erosion to a stable, grassed disposal area.Used to transport water to an erosion control structure such as a trickle pipe/grass chute.</p>
<h2>Limitations</h2>
<p>Not suitable for sites prone to tunnel erosion or sites where deep cracking, sub- surface seepage or tunnelling is occurring.Not suitable for sites with perennial water flow or high salt levels.</p>
<h2>Comments</h2>
<p>Diversion banks require skilled operators to ensure the grade is correct and the bank doesn't pond water. It is critical that the topsoil is removed and replaced over finished works. Moist soil is essential if adequate compaction is to be achieved.</p>

</div>
</div>

<div id="footer">
<?php require("$toroot/scripts/footer.php"); ?>
</div>

</body>
</html>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
<title>The Soil &amp; Land Portal</title>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" />
<meta name="MSSmartTagsPreventParsing" content="true" />

<meta name="author" content="Ben Sturmfels" />

<link rel="stylesheet" href="../simple.css" type="text/css" />
<style type="text/css" media="all">@import "../complex.css";</style>
<link rel="stylesheet" href="../print.css" type="text/css" media="print" />

<script type="text/javascript" src="../scripts/menu.js"></script>
</head>

<body>
<div class="hide"><a href="#content" title="Skip navigation." accesskey="2">Skip navigation</a>.</div>

<div id="header">
<?php 
$toroot = "..";
require("$toroot/scripts/header.php");
?>
</div>

<div id="wrapper">
<div id="nav">
<?php require("$toroot/scripts/menu.php"); ?>
</div>

<div id="content">
<h1>Gully Head Structure - Pipe</h1>

<div class="figleft"><img src="images/armcobefore.jpg" width="240" height="160" alt="armcobefore.jpg - 9255 Bytes" /></div>

<div class="figleft"><img src="images/armcoafter.jpg" width="240" height="310" alt="armcoafter.jpg - 17874 Bytes" /></div>

<h2>Description</h2>
<p>Large pipe installed at the head of a gully with a concrete inlet pit and headwall. Pipe can be polythene, aluminium or galvanised steel. Cost $6,000 - $15,000.</p>
<h2>Application</h2>
<p>Pipes have the ability to "hang" out over the gully head. They are commonly used in sites where the bed of the gully is unstable or soil conditions make the site unsuitable for other options.</p>
<h2>Limitations</h2>
<p>Pipe structures have a limited life due to abrasion, corrosion and exposure to sunlight. Galvanised steel pipes are generally not recommended.</p>
<h2>Comments</h2>
<p>Pipe diameters commonly used for erosion control range from 600mm to 1500mm.Important to allow extra pipe length to compensate for deepening of the gully head.</p>

</div>
</div>

<div id="footer">
<?php require("$toroot/scripts/footer.php"); ?>
</div>

</body>
</html>

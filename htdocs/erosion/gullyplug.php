<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
<title>The Soil &amp; Land Portal</title>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" />
<meta name="MSSmartTagsPreventParsing" content="true" />

<meta name="author" content="Ben Sturmfels" />

<link rel="stylesheet" href="../simple.css" type="text/css" />
<style type="text/css" media="all">@import "../complex.css";</style>
<link rel="stylesheet" href="../print.css" type="text/css" media="print" />

<script type="text/javascript" src="../scripts/menu.js"></script>
</head>

<body>
<div class="hide"><a href="#content" title="Skip navigation." accesskey="2">Skip navigation</a>.</div>

<div id="header">
<?php 
$toroot = "..";
require("$toroot/scripts/header.php");
?>
</div>

<div id="wrapper">
<div id="nav">
<?php require("$toroot/scripts/menu.php"); ?>
</div>

<div id="content">
<h1>Gully Plug Dam</h1>

<div class="figleft"><img src="images/plugbefore.jpg" width="240" height="158" alt="gebefore.jpg - 10076 Bytes" /><p style="width: 240px;">Before</p></div>

<div class="figleft"><img src="images/plugafter.jpg" width="240" height="159" alt="geafter.jpg - 10615 Bytes" />
<p style="width: 240px;">After</p></div>

<h2>Description</h2>
<p>A compacted earthen bank built across an eroded channel using a bulldozer, scraper or scoop. $2 - $3 per cubic metre of bank.</p>
<h2>Application</h2>
<p>Used to collect water off long stretches of gully battering and to transport overland flow across the eroded channel for disposal.</p>
<h2>Limitations</h2>
<p>A compacted earthen bank built across an eroded channel using a bulldozer, scraper or scoop.</p>
<h2>Comments</h2>
<p>Normally built with relatively short lifespan. Once gully battering has settled and grassed up gully plug can be replaced with a normal diversion bank.</p>

</div>
</div>

<div id="footer">
<?php require("$toroot/scripts/footer.php"); ?>
</div>

</body>
</html>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
<title>The Soil &amp; Land Portal</title>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" />
<meta name="MSSmartTagsPreventParsing" content="true" />

<meta name="author" content="Ben Sturmfels" />

<link rel="stylesheet" href="../simple.css" type="text/css" />
<style type="text/css" media="all">@import "../complex.css";</style>
<link rel="stylesheet" href="../print.css" type="text/css" media="print" />

<script type="text/javascript" src="../scripts/menu.js"></script>
</head>

<body>
<div class="hide"><a href="#content" title="Skip navigation." accesskey="2">Skip navigation</a>.</div>

<div id="header">
<?php 
$toroot = "..";
require("$toroot/scripts/header.php");
?>
</div>

<div id="wrapper">
<div id="nav">
<?php require("$toroot/scripts/menu.php"); ?>
</div>

<div id="content">
<h1>Erosion Control Techniques</h1>
<p>Listed below is a range of erosion control techniques used on a regular basis in the Upper Wimmera Catchment. The list is by no means exhaustive, limited topography &amp; climate, notes are fairly basic. For more detail the reader should consult one of the references listed on this site. On many of the larger projects a number of these techniques are used together to achieve a suitable outcome.</p>
<ul>
<li><a href="diversion.php">Diversion Banks</a></li>
<li><a href="gullybatt.php">Gully Battering</a></li>
<li><a href="gullyedge.php">Gully Edging</a></li>
<li><a href="gullyplug.php">Gully Plug Dam</a></li>
<li><a href="tp_chute.php">Trickle Pipe/Grass Chute</a></li>
<li><a href="pipehead.php">Gully Head Structure - Pipe</a></li>
<li><a href="drophead.php">Gully Head Structure - Concrete Drop/Chute</a></li>
<li><a href="dropgrade.php">Grade Control Structure - Concrete Drop</a></li>
<li><a href="rockgrade.php">Grade Control Structure - Rock Chute</a></li>
<li><a href="floodret.php">Flood Retention Dam</a></li>
</ul>
</div>

</div>

<div id="footer">
<?php require("$toroot/scripts/footer.php"); ?>
</div>

</body>
</html>

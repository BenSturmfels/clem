<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
<title>The Soil &amp; Land Portal</title>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" />
<meta name="MSSmartTagsPreventParsing" content="true" />

<meta name="author" content="Ben Sturmfels" />

<link rel="stylesheet" href="../simple.css" type="text/css" />
<style type="text/css" media="all">@import "../complex.css";</style>
<link rel="stylesheet" href="../print.css" type="text/css" media="print" />

<script type="text/javascript" src="../scripts/menu.js"></script>
</head>

<body>
<div class="hide"><a href="#content" title="Skip navigation." accesskey="2">Skip navigation</a>.</div>

<div id="header">
<?php 
$toroot = "..";
require("$toroot/scripts/header.php");
?>
</div>

<div id="wrapper">
<div id="nav">
<?php require("$toroot/scripts/menu.php"); ?>
</div>

<div id="content">
<h1>Concrete Drop Structure</h1>

<div class="figleft"><img src="images/siltbefore.jpg" width="240" height="160" alt="siltbefore.jpg - 13533 Bytes" /></div>

<div class="figleft"><img src="images/siltafter.jpg" width="240" height="158" alt="siltafter.jpg - 11580 Bytes" /></div>

<h2 style="clear: left">Description</h2>
<p>These structures are similar in design to gully head structures. Usually built as a free standing wall. Drop usually limited to 3 metres. Cost $20,000 plus.</p>
<h2>Application</h2>
<p>Used to control active bed erosion. Often installed to protect an upstream asset such as a bridge or culvert.</p>
<h2>Limitations</h2>
<p>Drop height is usually limited by channel capacity.</p>
<p>Requires rock bar or stable channel downstream to prevent undermining.</p>
<p>Forms a barrier to fish and other aquatic fauna.</p>
<h2>Comments</h2>
<p>Expensive to build because of difficult site conditions. Requires deep foundations into floor and banks to prevent failure.</p>

</div>
</div>

<div id="footer">
<?php require("$toroot/scripts/footer.php"); ?>
</div>

</body>
</html>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
<title>The Soil &amp; Land Portal</title>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" />
<meta name="MSSmartTagsPreventParsing" content="true" />

<meta name="author" content="Ben Sturmfels" />

<link rel="stylesheet" href="../simple.css" type="text/css" />
<style type="text/css" media="all">@import "../complex.css";</style>
<link rel="stylesheet" href="../print.css" type="text/css" media="print" />

<script type="text/javascript" src="../scripts/menu.js"></script>
</head>

<body>
<div class="hide"><a href="#content" title="Skip navigation." accesskey="2">Skip navigation</a>.</div>

<div id="header">
<?php 
$toroot = "..";
require("$toroot/scripts/header.php");
?>
</div>

<div id="wrapper">
<div id="nav">
<?php require("$toroot/scripts/menu.php"); ?>
</div>

<div id="content">
<h1>Trickle Pipe/Grass Chute</h1>

<div class="figleft"><img src="images/tpbefore.jpg" width="240" height="161" alt="tpbefore.jpg - 15603 Bytes" /><p style="width: 240px;">Before</p></div>

<div class="figleft"><img src="images/tpafter.jpg" width="240" height="161" alt="tpafter.jpg - 11101 Bytes" /><p style="width: 240px;">After</p></div>

<h2>Description</h2>
<p>A pipe and grassed ramp used to transfer water to the bed of a gully. The pipe, commonly 150mm PVC runs from the top of the gully head to the floor. A grass chute is a carefully prepared grassed slope with a grade of about 1:5. Cost $1,000 - $1,500.</p>
<h2>Application</h2>
<p>A Trickle Pipe/Grass Chute arrangement is used to control headward erosion. The trickle pipe carry's the regular daily flow while the grass chute handles the bigger flood flows.</p>
<h2>Limitations</h2>
<p>Usually limited to catchments with an area of 40 hectares and gullies less than 3 metres deep.Grass chutes requires regular maintenance.Not suitable in highly saline sites.</p>
<h2>Comments</h2>
<p>Grass chute must be constructed on consolidated material.Trickle pipe must have anti seepage baffles installed to prevent tunnelling along the pipe.</p>

</div>
</div>

<div id="footer">
<?php require("$toroot/scripts/footer.php"); ?>
</div>

</body>
</html>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
<title>The Soil &amp; Land Portal</title>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" />
<meta name="MSSmartTagsPreventParsing" content="true" />

<meta name="author" content="Ben Sturmfels" />

<link rel="stylesheet" href="../simple.css" type="text/css" />
<style type="text/css" media="all">@import "../complex.css";</style>
<link rel="stylesheet" href="../print.css" type="text/css" media="print" />

<script type="text/javascript" src="../scripts/menu.js"></script>
</head>

<body>
<div class="hide"><a href="#content" title="Skip navigation." accesskey="2">Skip navigation</a>.</div>

<div id="header">
<?php 
$toroot = "..";
require("$toroot/scripts/header.php");
?>
</div>

<div id="wrapper">
<div id="nav">
<?php require("$toroot/scripts/menu.php"); ?>
</div>

<div id="content">
<h1>Gully Edging</h1>

<div class="figleft"><img src="images/gebefore.jpg" width="240" height="158" alt="gebefore.jpg - 10076 Bytes" /><p style="width: 240px;">Before</p></div>
<div class="figleft"><img src="images/geafter.jpg" width="240" height="158" alt="geafter.jpg - 10615 Bytes" />
<p style="width: 240px;">After</p></div>

<h2>Description</h2>
<p>An excavator is used to form a smooth even batter. The process involves stripping off topsoil, battering down the bank and respreading the topsoil over the finished surface. Finished batter slopes range from 2:1 to 3:1.Cost $3 - $10 per lineal metre.</p>
<h2>Application</h2>
<p>This technique is used to control various forms of stream bank erosion such as undermining, sapping and sheet erosion.</p>
<h2>Limitations</h2>
<p>Not suitable for immature gullies or streams where the channel is too narrow to carry the required flow.Requires highly skilled operator to be done efficiently.Early storms can cause erosion of batter toe.</p>
<h2>Comments</h2>
<p>In gullies or streams with vertical banks excess material may need to be spread on adjacent land. Some channel straightening may be required to prevent erosion of batter toe. The batters need to be sown down immediately works have been finished.</p>

</div>
</div>

<div id="footer">
<?php require("$toroot/scripts/footer.php"); ?>
</div>

</body>
</html>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
<title>The Soil &amp; Land Portal</title>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" />
<meta name="MSSmartTagsPreventParsing" content="true" />

<meta name="author" content="Ben Sturmfels" />

<link rel="stylesheet" href="../simple.css" type="text/css" />
<style type="text/css" media="all">@import "../complex.css";</style>
<link rel="stylesheet" href="../print.css" type="text/css" media="print" />

<script type="text/javascript" src="../scripts/menu.js"></script>
</head>

<body>
<div class="hide"><a href="#content" title="Skip navigation." accesskey="2">Skip navigation</a>.</div>

<div id="header">
<?php
$toroot = "..";
require("$toroot/scripts/header.php");
?>
</div>

<div id="wrapper">
<div id="nav">
<?php require("$toroot/scripts/menu.php"); ?>
</div>

<div id="content">
<h1>Videos</h1>
<h2>Aggregate Stability - Dry And Moist Soil Sample</h2>
<video src="Aggregate Stability - Dry And Moist Soil Sample.mp4" controls="true" style="width: 100%; height: auto;"></video>

<h2 style="margin-top: 1rem;">Aggregate Stability Test Dry Sample</h2>
<video src="Aggregate Stability Test Dry Sample.mp4" controls="true" style="width: 100%; height: auto;"></video>

</div>

</div>

<div id="footer">
<?php require("$toroot/scripts/footer.php"); ?>
</div>

</body>
</html>

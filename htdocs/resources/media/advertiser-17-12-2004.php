<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
<title>The Soil &amp; Land Portal</title>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" />
<meta name="MSSmartTagsPreventParsing" content="true" />

<meta name="author" content="Ben Sturmfels" />

<link rel="stylesheet" href="../../simple.css" type="text/css" />
<style type="text/css" media="all">@import "../../complex.css";</style>
<link rel="stylesheet" href="../../print.css" type="text/css" media="print" />

<script type="text/javascript" src="../../scripts/menu.js"></script>
</head>

<body>
<div class="hide"><a href="#content" title="Skip navigation." accesskey="2">Skip navigation</a>.</div>

<div id="header">
<?php
$toroot = "../..";
require("$toroot/scripts/header.php");
?>
</div>

<div id="wrapper">
<div id="nav">
<?php require("$toroot/scripts/menu.php"); ?>
</div>

<div id="content">
<h1>Erosion as banks burst</h1>
<h2><a href="http://www.ararat.yourguide.com.au/">The Ararat Advertiser</a>, Dec 17th 2004</h2>

<div class="figright"><a href="images/flooding-001.jpg"><img src="images/thumbs/flooding-001.jpg" /></a></div>
<p>Weekend stroms caused major damage to roads, houses, fences and infrastructure throughout the region. The storm also resulted in significant soil erosion problems on district properties with many tons of soil being washed into nearby waterways, namely the Hopkins River.</p>
<p>Department of Primary Industries Soil Conservation Officer Clem Sturmfels said that storms such as these were not uncommon over the summer months. "Such storms, normally only affect a very small area of the countryside. Unfortunately, this time it hit a more populated area, he said.</p>
<div class="figleft"><a href="images/flooding-002.jpg"><img src="images/thumbs/flooding-002.jpg" /></a></div>
<p>One of the most affected areas was the Bellinghams Hill Road, north west of Mt Chalembar. Nearby property owner Richard Keys said the storm dumped 49.5mm of rain in their gauge in just over an hour.</p>
<p>The intense rain fall caused massive quantites of sand, silt and gravel to be washed downstream into the local river system. Mr Sturmfels said according to his calculations the storm was about a one in one-hundred year event.</p>
<p>Mr Keys said the storm was the worst he'd experienced in the twenty years he'd been on the property. He said the damage from the storm was still obvious. Mr Keys said the rain was counterproductive in terms of its agricultural benefits.</p>
<p>"Some people think that farmers want rain all the time but it's only productive if it falls at the right time," he said.</p>
<p>The weekend storms also caused the flooding of several public housing units in Tobin St with twelve residents evacuated.</p>



<div style="clear: left">&nbsp;</div>

</div>
</div>

<div id="footer">
<?php require "$toroot/scripts/footer.php"; ?>
</div>

</body>
</html>
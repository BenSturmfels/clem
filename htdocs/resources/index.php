<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
<title>The Soil &amp; Land Portal</title>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" />
<meta name="MSSmartTagsPreventParsing" content="true" />

<meta name="author" content="Ben Sturmfels" />

<link rel="stylesheet" href="../simple.css" type="text/css" />
<style type="text/css" media="all">@import "../complex.css";</style>
<link rel="stylesheet" href="../print.css" type="text/css" media="print" />

<script type="text/javascript" src="../scripts/menu.js"></script>
</head>

<body>
<div class="hide"><a href="#content" title="Skip navigation." accesskey="2">Skip navigation</a>.</div>

<div id="header">
<?php
$toroot = "..";
require("$toroot/scripts/header.php");
?>
</div>

<div id="wrapper">
<div id="nav">
<?php require("$toroot/scripts/menu.php"); ?>
</div>

<div id="content">
<h1>Resources</h1>
<h2>Workshop notes</h2>
<ul>
<li><a href="Dixons%20Creek%20Workshop%20Notes%20Final%201%20October%202010_dw.pdf">Dixons Creek Workshop Notes, June 2010</a></li>
<li><a href="workshop-2005-2/surveying.pdf">Surveying</a></li>
<li><a href="workshop-2005-2/flow and jumps.pdf">Notch flow, pipe flow and hydraulic jumps</a></li>
</ul>

<h2>Landcare notes</h2>
<ul>
<li><a href="trickle pipe landcare notes.pdf">Trickle pipes</a></li>
</ul>

<h2>Structure plans</h2>
<ul>
<li><a href="diversion bank layout.pdf">Standard diversion bank plan</a></li>
<li><a href="rock chute layout 1.pdf">Rock chute layout 1</a></li>
<li><a href="rock chute layout 2.pdf">Rock chute layout 2</a></li>
<li><a href="gabion layout no floor.pdf">Gabion layout no floor</a></li>
<li><a href="gabion layout with floor.pdf">Gabion layout with floor</a></li>
</ul>

<ul>
<li><a href="Contract Prices Soil Conservation Structures.pdf">Structure contract prices</a></li>
<li><a href="Soil Conservation Earthwork Specification.pdf">Earthworks specifications</a></li>
</ul>

<h2>Technical notes</h2>
<ul><li><a href="flood discharge sheet.pdf">Flood discharge calculation sheet</a></li>
</ul>

<h2>Media releases</h2>
<ul>
<li><a href="media/advertiser-17-12-2004.php">Erosion as banks burst</a></li>
</ul>

<div style="clear: left">&nbsp;</div>

</div>
</div>

<div id="footer">
<?php require "$toroot/scripts/footer.php"; ?>
</div>

</body>
</html>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
<title>The Soil &amp; Land Portal</title>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" />
<meta name="MSSmartTagsPreventParsing" content="true" />

<meta name="author" content="Ben Sturmfels" />

<link rel="stylesheet" href="simple.css" type="text/css" />
<style type="text/css" media="all">@import "complex.css";</style>

<script type="text/javascript" src="scripts/menu.js"></script>
</head>

<body>
<div class="hide"><a href="#content" title="Skip navigation." accesskey="2">Skip navigation</a>.</div>

<div id="header">
<?php 
$toroot = ".";
require("$toroot/scripts/header.php");
?>
</div>

<div id="wrapper">
<div id="nav">
<?php require("scripts/menu.php"); ?>
</div>

<div id="content">

<p><q>Promoting and fostering the understanding, science and practice of Soil Conservation in South Western Victoria.</q></p>

<div class="figright" style="width: 200px"><a href="images/big-erosion.jpg"><img src="images/thumbs/big-erosion.jpg" width="200" height="354" alt="" /></a>
<p><a href="images/big-erosion.jpg">Gully Erosion, Wimmera River catchment</a></p></div>

<p>The site has been developed to assist landholders, landcare groups, extension officers and students to gain a better understand of the causes, process and control of soil erosion. Information used in the site has been prepared and collected by the author over the last 20 years in his role as a Soil Conservation Officer with the <a href="http://www.dpi.gov.au">Department of Primary Industries</a> (Formerly Natural Resources and Environment) while based at Ararat in South Western Victoria.</p>
<p>The author would like to acknowledge the support of other staff and local landholders in the development of this material. Comments, assistance or contributions of material are most welcome.</p>

<div class="figright" style="width: 250px; margin-top: 1em; clear: right;"><a href="images/morrows-gullies.jpg"><img src="images/thumbs/morrows-gullies2.jpg" width="250" height="164" alt="" /></a><p><a href="images/morrows-gullies.jpg">A few crops too many</a></p></div>

<h2>Recent workshop</h2>
<p>It's been great to have such positive feedback on the recent soil conservation workshop held in Ararat. The <a href="resources/">workshop notes</a> from these sessions are available, along with a selection of <a href="photos/workshop2-2005/">photos</a> and the surveying <a href="calculators/">spreadsheets</a>.</p>

<div style="clear: right">&nbsp;</div>
</div>

</div>

<div id="footer">
<?php require "$toroot/scripts/footer.php"; ?>
</div>

</body>
</html>

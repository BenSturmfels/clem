<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN"
    "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>Soil Conservation Workshop, Ararat 2005</title>
<link rel="stylesheet" href="../../simple.css" type="text/css" />
<style type="text/css"><!-- 
	@import "../../complex.css"; 
	@import "../../album.css";
--></style>
<link rel="shortcut icon" type="image/ico" href="../../icon.ico" />
</head>
<body>
<h1>Soil Conservation Workshop, Ararat 2005</h1>
<div class="gallery">
<div class="photo">
<a href="images/2005-05-30m07-12-56-1.jpg"><img src="images/thumb-2005-05-30m07-12-56-1.jpg" width="180" height="120" alt="" /></a>
<p></p>
</div>
<div class="photo">
<a href="images/2005-05-30m07-12-57-1.jpg"><img src="images/thumb-2005-05-30m07-12-57-1.jpg" width="181" height="120" alt="" /></a>
<p></p>
</div>
<div class="photo">
<a href="images/2005-05-30m07-12-55-1.jpg"><img src="images/thumb-2005-05-30m07-12-55-1.jpg" width="180" height="120" alt="" /></a>
<p></p>
</div>
<div class="photo">
<a href="images/2005-05-30m07-12-55-2.jpg"><img src="images/thumb-2005-05-30m07-12-55-2.jpg" width="181" height="120" alt="" /></a>
<p></p>
</div>
<div class="photo">
<a href="images/2005-05-30m07-12-56-2.jpg"><img src="images/thumb-2005-05-30m07-12-56-2.jpg" width="180" height="120" alt="" /></a>
<p></p>
</div>
<div class="photo">
<a href="images/2005-05-30m07-12-56-3.jpg"><img src="images/thumb-2005-05-30m07-12-56-3.jpg" width="181" height="120" alt="" /></a>
<p></p>
</div>
<div class="photo">
<a href="images/2000-10-20+09-58-53.jpg"><img src="images/thumb-2000-10-20+09-58-53.jpg" width="181" height="120" alt="" /></a>
<p></p>
</div>
<div class="photo">
<a href="images/2005-05-30m07-12-57-2.jpg"><img src="images/thumb-2005-05-30m07-12-57-2.jpg" width="181" height="120" alt="" /></a>
<p></p>
</div>
<div class="photo">
<a href="images/2005-05-30m07-12-54-1.jpg"><img src="images/thumb-2005-05-30m07-12-54-1.jpg" width="181" height="120" alt="" /></a>
<p></p>
</div>
<div class="photo">
<a href="images/2005-05-30m07-13-11-1.jpg"><img src="images/thumb-2005-05-30m07-13-11-1.jpg" width="180" height="120" alt="" /></a>
<p></p>
</div>
<div class="photo">
<a href="images/2005-05-30m07-12-54-2.jpg"><img src="images/thumb-2005-05-30m07-12-54-2.jpg" width="181" height="120" alt="" /></a>
<p></p>
</div>
<div class="photo">
<a href="images/2005-05-30m07-13-12-1.jpg"><img src="images/thumb-2005-05-30m07-13-12-1.jpg" width="181" height="120" alt="" /></a>
<p></p>
</div>
<div class="photo">
<a href="images/2005-05-30m07-13-12-2.jpg"><img src="images/thumb-2005-05-30m07-13-12-2.jpg" width="181" height="120" alt="" /></a>
<p></p>
</div>
<div class="photo">
<a href="images/2005-05-30m07-13-13-1.jpg"><img src="images/thumb-2005-05-30m07-13-13-1.jpg" width="180" height="120" alt="" /></a>
<p></p>
</div>
<div class="photo">
<a href="images/2005-05-30m07-13-13-2.jpg"><img src="images/thumb-2005-05-30m07-13-13-2.jpg" width="181" height="120" alt="" /></a>
<p></p>
</div>
<div class="photo">
<a href="images/2005-05-30m07-13-13-3.jpg"><img src="images/thumb-2005-05-30m07-13-13-3.jpg" width="181" height="120" alt="" /></a>
<p></p>
</div>
<div class="photo">
<a href="images/2005-05-30m07-13-14-1.jpg"><img src="images/thumb-2005-05-30m07-13-14-1.jpg" width="181" height="120" alt="" /></a>
<p></p>
</div>
<div class="photo">
<a href="images/2005-05-30m07-13-14-2.jpg"><img src="images/thumb-2005-05-30m07-13-14-2.jpg" width="181" height="120" alt="" /></a>
<p></p>
</div>
<div class="photo">
<a href="images/2005-05-30m07-13-10.jpg"><img src="images/thumb-2005-05-30m07-13-10.jpg" width="180" height="120" alt="" /></a>
<p></p>
</div>
<div class="photo">
<a href="images/2005-05-30m07-12-45-1.jpg"><img src="images/thumb-2005-05-30m07-12-45-1.jpg" width="181" height="120" alt="" /></a>
<p></p>
</div>
<div class="photo">
<a href="images/2005-05-30m07-13-11-2.jpg"><img src="images/thumb-2005-05-30m07-13-11-2.jpg" width="181" height="120" alt="" /></a>
<p></p>
</div>
<div class="photo">
<a href="images/2005-05-30m07-12-44-1.jpg"><img src="images/thumb-2005-05-30m07-12-44-1.jpg" width="181" height="120" alt="" /></a>
<p></p>
</div>
<div class="photo">
<a href="images/2005-05-30m07-12-44-2.jpg"><img src="images/thumb-2005-05-30m07-12-44-2.jpg" width="181" height="120" alt="" /></a>
<p></p>
</div>
<div class="photo">
<a href="images/2005-05-30m07-12-44-3.jpg"><img src="images/thumb-2005-05-30m07-12-44-3.jpg" width="181" height="120" alt="" /></a>
<p></p>
</div>
<div class="photo">
<a href="images/2005-05-30m07-12-45-2.jpg"><img src="images/thumb-2005-05-30m07-12-45-2.jpg" width="181" height="120" alt="" /></a>
<p></p>
</div>
<div class="photo">
<a href="images/2005-05-30m07-12-43.jpg"><img src="images/thumb-2005-05-30m07-12-43.jpg" width="181" height="120" alt="" /></a>
<p></p>
</div>
</div>
<?php
$path2root = '../..';

include '../footer.php';
?>
</body>
</html>
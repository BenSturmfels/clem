<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
<title>The Soil &amp; Land Portal</title>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" />
<meta name="MSSmartTagsPreventParsing" content="true" />

<meta name="author" content="Ben Sturmfels" />

<link rel="stylesheet" href="simple.css" type="text/css" />
<style type="text/css" media="all">@import "complex.css";</style>

<script type="text/javascript" src="scripts/menu.js"></script>
</head>

y<body>
<div class="hide"><a href="#content" title="Skip navigation." accesskey="2">Skip navigation</a>.</div>

<div id="header">
<?php 
$toroot = ".";
require("$toroot/scripts/header.php");
?>
</div>

<div id="wrapper">
<div id="nav">
<?php require("$toroot/scripts/menu.php"); ?>
</div>

<div id="content">
<h1>Links</h1>
<ul>
<li><a href="http://www.dpi.vic.gov.au">Department of Primary Industries</a></li>
<li><a href="http://www.dse.vic.gov.au">Department of Sustainability and Environment</a></li>
<li><a href="http://www.treeproject.asn.au">TreeProject</a></li>
<li><a href="http://www.saltwatch.org.au">Saltwatch</a></li>
<li><a href="http://www.sheppstc.org.au/srco/saltwatch.htm">Salinity Resource Centre</a></li>
<li><a href="http://www.affa.gov.au/docs/nrm/landcare/nlp.html">National Landcare Program</a></li>
<li><a href="http://www.lmsinfo.com">Land Management Society</a></li>
<li><a href="http://www.landcareweb.com">Landcare Web</a></li>
</ul>
</div>
</div>

<div id="footer">
<?php require "$toroot/scripts/footer.php"; ?>
</div>

</body>
</html>

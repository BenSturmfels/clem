function code(text, rotation) {
    var out = '';
    for(i=0; i < text.length; i++){
        var a = text.charCodeAt(i) + rotation;
        out = out.concat(String.fromCharCode(a));
    }
    return out;
}

function decode(ciph) {
    return code(ciph, -3);
}

function encode(text) {
    return code(text, +3);
}

document.write(decode('?d#kuhi@%pdlowr=fohp1vwxupihovCdjulfxowxuh1ylf1jry1dx%AFrqwdfw#Fohp?2dA'));

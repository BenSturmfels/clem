<?php $doctype = '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">\n';

$html = '<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">\n';

$headcontent = '<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" />
<meta name="MSSmartTagsPreventParsing" content="true" />

<meta name="author" content="Ben Sturmfels" />

<link rel="stylesheet" href="simple.css" type="text/css" />
<style type="text/css" media="all">@import "complex.css";</style>

<script type="text/javascript" src="scripts/menu.js"></script>\n';

$skipnav = '<div class="hide"><a href="#content" title="Skip navigation." accesskey="2">Skip navigation</a>.</div>';

$header = '<div id="header">
<div id="title"><a href="<?php $toroot ?>">Soil, Land &amp; Water</a></div>
<div id="byline">Clem Sturmfels</div>
</header>
<div id="wrapper">\n';

$nav = '<div id="nav">
<ul class="menu">
<li><a href="$toroot/index.php">Home</a></li>
<li><a href="$toroot/projects/index.php">Projects</a></li>
<li class="submenu" onmouseover="showSub('erosion');" onmouseout="hideSub('erosion');"><a href="$toroot/erosion/index.php" title="Erosion Control Techniques">Erosion Control</a><ul onmouseover="stayOpen(this);" id="erosion">
	  <li><a href="$toroot/erosion/diversion.php">Diversion Banks</a></li>
	  <li><a href="$toroot/erosion/gullybatt.php">Gully Battering</a></li>
	  <li><a href="$toroot/erosion/gullyedge.php">Gully Edging</a></li>
	  <li><a href="$toroot/erosion/gullyplug.php">Gully Plug Dam</a></li>
	  <li><a href="$toroot/erosion/tp_chute.php">Trickle Pipe/Grass Chute</a></li>
	  <li><a href="$toroot/erosion/pipehead.php">Gully Head Structure - Pipe</a></li>
	  <li><a href="$toroot/erosion/drophead.php">Gully Head Structure - Concrete Drop/Chute</a></li>
	  <li><a href="$toroot/erosion/dropgrade.php">Grade Control Structure - Concrete Drop</a></li>
	  <li><a href="$toroot/erosion/rockgrade.php">Grade Control Structure - Rock Chute</a></li>
	  <li><a href="$toroot/erosion/floodret.php">Flood Retention Dam</a></li>
	</ul>
</li><li><a href="$toroot/calculators/flow.php">Flow Calculator</a></li><li><a href="$toroot/links.php" title="External Links">Links</a></li>
</ul>
</div>\n';

?>

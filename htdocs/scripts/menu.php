<?php
if (!isset($toroot)) $toroot = ".";

echo "<ul class=\"menu\">
<li><a href=\"$toroot/\">Home</a></li>
<li><a href=\"$toroot/resources/\">Resources</a></li>
<li class=\"submenu\"><a href=\"$toroot/erosion/\" title=\"Erosion Control Techniques\">Erosion control</a>
<li><a href=\"$toroot/projects/\">Projects</a></li>
<li><a href=\"$toroot/calculators/\">Calculators</a></li>
<li><a href=\"$toroot/links.php\" title=\"External Links\">Links</a></li>
<li><a href=\"$toroot/videos/\">Videos</a></li>
</ul>";
?>
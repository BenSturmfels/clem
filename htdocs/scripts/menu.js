/*
   Author: Ben Sturmfels, design@stumbles.id.au
   Description: Popout menu script for nested <ul>s
   Last modified: 17 Jan 2004
*/

/* showSub -- Runs on onmouseover() event for top level menus 
   to open the submenu */
function showSub(id) {
	element = document.getElementById(id);
	// Cancel close event if we've moved back from its submenu
	clearTimeout(element.timerID);
	// Make sure its visible
	element.style.visibility = "visible";
}

/* hideSub -- Runs on onmouseout() event for the top level menus 
   to close the submenu */
function hideSub(id) {
	// We've just left the menu, set a close event.
	// Each have individual timers so they only cancel themselves
	document.getElementById(id).timerID 
		= setTimeout("close('" + id + "')",250);	
}

/* close -- Scheduled to run when we leave a top-level, or submenu */
function close(id) {
	// Close the menu
	document.getElementById(id).style.visibility = "hidden";
}

/* stayOpen -- Runs on onmouseover() on a submenu to hold it open
   so that the timer event doesn't close it */
function stayOpen(element) {
	// Cancel close event as we're now in its submenu
	clearTimeout(element.timerID);
}
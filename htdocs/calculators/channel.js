'use strict';

// TODO: Repaint at regular interval if data has changed, rather than repainting
// immediately as a change occurs.
// https://developer.mozilla.org/en-US/docs/Web/API/Canvas_API/A_basic_ray-caster

// TODO: There's unused dependent variables. Display them?

// TODO: Add some tests to make sure calculations are correct.

// TODO: Validate the input arguments are sensible.

// TODO: Vertically centre the diagram?

var channelColor = '#633';

// Independent variables, distances in metres.
var w1, w2, cd, s, m, fd;

// Dependent variables.
var fw, a, p, r, v, q;

window.onload = function () {
    document.mannings.addEventListener('input', draw);
    draw();
};

function draw() {
    w1 = document.mannings.w1.valueAsNumber;
    w2 = document.mannings.w2.valueAsNumber;
    cd = document.mannings.cd.valueAsNumber;
    s = document.mannings.s.valueAsNumber;
    m = document.mannings.m.valueAsNumber;
    fd = document.mannings.fd.valueAsNumber;

    console.log('w1: ' + w1);
    console.log('w2: ' + w2);

    // Flow width.
    fw = w2 + (fd * (w1 - w2)) / cd;
    // Area.
    a = fd * (w2 + (fw - w2) / 2);
    // Wetted perimeter.
    var temp = Math.pow((fw - w2) / 2, 2.0) + Math.pow(fd, 2.0);
    p = 2 * Math.sqrt(temp) + w2;
    // Hydraulic radius.
    // avoid dividing by zero - define answer as zero when fd == 0
    r = p == 0 ? 0 : a / p;
    // Velocity.
    v = (Math.pow(r, 2.0 / 3.0) * Math.pow(s, 0.5)) / m;
    // Flow.
    q = a * v;

    // TODO: Could also be done by directly attaching a listener to the s and m
    // elements. Probably more responsive.
    document.mannings.s_output.value = s;
    document.mannings.m_output.value = m;

    document.mannings.v.value = v.toFixed(2);
    document.mannings.q.value = q.toFixed(2);

    console.log('Flow width: ' + fw);
    console.log('Area: ' + a);
    console.log('Whetted perimeter: ' + p);
    console.log('Hydraulic radius: ' + r);
    console.log('Velocity: ' + v);
    console.log('Flow: ' + q);

    var canvas = document.getElementById('square');
    canvas.width = canvas.width; // clear
    var ctx = canvas.getContext('2d');

    var maxsize = canvas.width - 2;
    console.log(maxsize);

    var scalefactor;
    var transX = 0;
    var transY = 0;

    // find largest of w1, w2 and cd
    if (w1 >= w2 && w1 >= cd) {
        scalefactor = maxsize / w1;
    }
    else if (w2 >= w1 && w2 >= cd) {
        scalefactor = maxsize / w2;
        // translate by 1/2 of difference between top and bottom
        transX = (w2 - w1) / 2;
    }
    else {
        scalefactor = maxsize / cd;
        if (w1 < w2) transX = (w2 - w1) / 2;
    }

    // get scaling factor by dividing panel size by largest value
    // apply transform with this factor

    ctx.scale(scalefactor, scalefactor);
    ctx.translate(transX, transY);

    // Draw water.
    ctx.beginPath();
    ctx.moveTo(w1 / 2 - fw / 2, cd - fd);
    ctx.lineTo(w1 / 2 + fw / 2, cd - fd);
    ctx.lineTo(w1 / 2 + w2 / 2, cd); // down left side
    ctx.lineTo(w1 / 2 - w2 / 2, cd); // across bottom
    var grd = ctx.createLinearGradient(0, 0, w1, cd);
    // light blue
    grd.addColorStop(0, '#8ED6FF');
    // dark blue
    grd.addColorStop(1, '#004CB3');
    ctx.fillStyle = grd;
    ctx.closePath();
    ctx.fill();

    // Draw channel.
    ctx.beginPath();
    ctx.moveTo(0, 0);
    ctx.lineTo(w1 / 2 - w2 / 2, cd); // down left side
    ctx.lineTo(w1 / 2 + w2 / 2, cd); // across bottom
    ctx.lineTo(w1, 0); // up right side
    ctx.strokeStyle = channelColor;
    ctx.lineWidth = 0.1;
    ctx.stroke();

}

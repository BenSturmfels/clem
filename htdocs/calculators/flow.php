<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
<title>The Soil &amp; Land Portal</title>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" />
<meta name="MSSmartTagsPreventParsing" content="true" />

<meta name="author" content="Ben Sturmfels" />

<link rel="stylesheet" href="../simple.css" type="text/css" />
<style type="text/css" media="all">@import "../complex.css";</style>
<link rel="stylesheet" href="../print.css" type="text/css" media="print" />

<script type="text/javascript" src="../scripts/menu.js"></script>
</head>

<body>
<div class="hide"><a href="#content" title="Skip navigation." accesskey="2">Skip navigation</a>.</div>

<div id="header">
<?php 
$toroot = "..";
require("$toroot/scripts/header.php");
?>
</div>

<div id="wrapper">
<div id="nav">
<?php require("$toroot/scripts/menu.php"); ?>
</div>

<div id="content">
<h1>Channel flow calculator</h1>
<p>This simple program calculates the flow of water in a rectangular or trapezoidal channel using Manning's formula: <img class="eqn" src="eqns/mannings.gif" width="156" height="46" alt="V=(R^(2/3)S^(1/2))/eta" />The formula is only applicable to channels that have a relatively uniform profile and slope.</p>
<h2>Method</h2>
<ol>
<li>Choose a straight reach of channel with a length at least 5 times the width, with constant slope.</li>
<li>Survey at least 3 cross sections to determine average channel dimensions.</li>
<li>Survey the channel bed (as an estimation of the water surface level) to determine the energy gradient (slope) of the reach.
<li>Estimate a value for Manning's roughness coefficient <img style="display: inline" src="eqns/eta.gif" width="13" height="16" alt="eta" /></li>
<li>Enter channel dimensions into calculator to determine <strong>velocity</strong> and <strong>unit discharge</strong>.</li>
</ol>
<applet code="Interface.class" archive="applets/channel.jar" width="400" height="300"></applet>

<h2>Can't see the flow calculator?</h2>
<p>User within the Department of Primary Industries or other departments may see a blank rectangle in place of the channel flow calculator. Regardless of web-browser used, you will need <a href="http://www.java.com/en/download/manual.jsp">download and install an updated version of Java</a>.</p>

<p>The problem occurs because the version of Java packaged with Netscape 4.7 is outdated. Internet Explorer will also used this version by default.</p>
</div>
</div>

<div id="footer">
<?php require "$toroot/scripts/footer.php"; ?>
</div>

</body>
</html>

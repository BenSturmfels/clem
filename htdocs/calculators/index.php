<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
<title>The Soil &amp; Land Portal</title>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" />
<meta name="MSSmartTagsPreventParsing" content="true" />

<meta name="author" content="Ben Sturmfels" />

<link rel="stylesheet" href="../simple.css" type="text/css" />
<style type="text/css" media="all">@import "../complex.css";</style>
<link rel="stylesheet" href="../print.css" type="text/css" media="print" />

<script type="text/javascript" src="../scripts/menu.js"></script>
</head>

<body>
<div class="hide"><a href="#content" title="Skip navigation." accesskey="2">Skip navigation</a>.</div>

<div id="header">
<?php 
$toroot = "..";
require("$toroot/scripts/header.php");
?>
</div>

<div id="wrapper">
<div id="nav">
<?php require("$toroot/scripts/menu.php"); ?>
</div>

<div id="content">
<h1>Calculators &amp; spreadsheets</h1>
<h2><a href="../channel-flow/">Channel flow calculator</a></h2>
<p>This online tool calculates the flow of water in a rectangular or trapezoidal channel</p>

<h2><a href="../bank-volume/">Dam bank volume calculator</a></h2>
<p>This calculator uses the "average end area" method to determine the volume of a trapezoidal shaped dam bank.</p>

<h2><a href="spreadsheets/level reduction.xls">Level reduction spreadsheet</a></h2>
<p>This simple "Excel" spreadsheet calculates
reduced levels from raw survey data using the "rise and fall" method. Data
is entered in the same way as with a traditional survey book. The data
would normally originate from a dumpy or laser level.</p>

<h2><a href="spreadsheets/tacheometric reduction.xls">Tacheometric reduction spreadsheet</a></h2>
<p>This "Excel" spreadsheet calculates
reduced levels and rectangular co-ordinates from a theodolite or similar
instrument. It requires data to be entered in the form of a horizontal
bearing with the degrees and minutes entered in separate columns. The
vertical angle is entered as decimal degrees, and the stadia readings
entered as "Top Hair", "Middle Hair" and "Bottom Hair" respectively. It is
important to note that the accuracy of the results are limited to a
horizontal bearing of 1'. The station reduced level and instrument height
can also be entered into the spreadsheet.</p>

<h2><a href="spreadsheets/dam bank volume.xls">Dam bank volume spreadsheet</a></h2>
<p>This "Excel" spreadsheet calculates the compacted
volume of an earthen bank using the "End Area" method. The elevation along
the centre-line of the bank is entered into the spreadsheet from one end of
the bank to the other. The respective distance between each elevation is
entered in the second column. Other data required is the slope of the
upstream and downstream bank batters, the width of the top of the bank
("crest width") and the elevation of the top of the bank ("crest level").
It is important to note that the elevation data can be entered in as a
level reading or in the reduced level format.</p>

<h2><a href="spreadsheets/stadia reduction.xls">Stadia reduction spreadsheet</a></h2>
<p>This "Excel" spreadsheet calculates reduced
levels and rectangular co-ordinates from raw survey data. The raw data is
entered in the same way as in a traditional survey book with the stadia
readings entered in the "Top Hair", "Bottom Hair" and "Bearing" columns
respectively. The Horizontal bearing ("Bearing") needs to be entered in a
decimal format. Normally this spreadsheet would be used for entering data
from a dumpy level or similar instrument. Once the raw data has been
entered and rectangular co-ordinates calculated, some transposition of the
data may be required to avoid negative co-ordinates. This is simply
achieved by finding the largest negative figure in each or the "X" and "Y"
columns, and adding this as a positive figure to all the data in each
column respectively.</p>
</div>
</div>

<div id="footer">
<?php require "$toroot/scripts/footer.php"; ?>
</div>

</body>
</html>

import java.lang.Math;
import java.text.DecimalFormat;

class Channel {
    // Independant variables - distances are in metres
    private SettableDouble w1; // top width
    private SettableDouble w2; // bottom width
    private SettableDouble cd; // channel depth
    private SettableDouble s; // slope
    private SettableDouble m; // mannings roughness co-eff (no unit)
    private SettableDouble fd; // flowdepth

    // Dependant variables
    private boolean calculated; // Flag indicating whether the calculations have been successful
    private double fw1; // flow width
    private double a; // cross sectional area (m^2)
    private double p; // wetted perimeter
    private double r; // hyraulic radius
    private double v; // velocity (m/sec)
    private double q; // flow rate (m^3/sec)

    // Log of errors
    public String error_log;

    // Number formatting - used for printcalcs in testing
    private static String format = "0.00";
    private static DecimalFormat nf = new DecimalFormat(format);

    Channel() { 
	this.w1 = new SettableDouble();
	this.w2 = new SettableDouble();
	this.cd = new SettableDouble();
	this.s = new SettableDouble();
	this.m = new SettableDouble();
	this.fd = new SettableDouble();
	calculated = false; // no parameters have been given yet
    }

    Channel(double w1, double w2, double cd, double s, 
		   double m, double fd){
	this.w1 = new SettableDouble();
	this.w2 = new SettableDouble();
	this.cd = new SettableDouble();
	this.s = new SettableDouble();
	this.m = new SettableDouble();
	this.fd = new SettableDouble();
	setParameters(w1, w2, cd, s, m, fd);
    }

    
    /** Set independant variables */
    public void setParameters(double w1, double w2, double cd,
			      double s, double m, double fd) {
	System.out.println("Set parameters called with: " + w1 + " " + w2 + " " + cd + " " + s + " " + m +  " " + fd);
	
	// Validation of input parameters
	error_log = "";
	// Top and Bottom widths
	if(w1 < w2 && w1 <= 0) {
	    error_log += "Top width must be greater than zero and greater than bottom width\n";
	    this.w1.unset();
	}
	else if(w1 <= 0) {
	    error_log += "Top width must be greater than zero\n";
	    this.w1.unset();
	}
	else if(w1 < w2) {
	    error_log += "Top width must be greater than bottom width\n";
	    this.w1.unset();
	    this.w2.unset();
	}
	else {
	    this.w1.set(w1);
	}	
	
	if (w2 < 0) {
	    error_log += "Bottom width must be greater than or equal to zero\n";
	    this.w2.unset();
	}
	else {
	    this.w2.set(w2);
	}

	// Channel depth
	if (cd <= 0) {
	    error_log += "Channel depth must be positive\n";
	    this.cd.unset();
	}
	else {
	    this.cd.set(cd);
	}

	// Slope
	if (s <= 0 || s > 1) {
	    error_log += "Slope must be greater than zero and less than or equal to one\n";
	    this.s.unset();
	}
	else {
	    this.s.set(s);
	}

	// Mannings
	if (m < 0.01 || m > 0.10) {
	    error_log += "Mannings must be between 0.01 and 0.10 inclusive\n";
	    this.m.unset();
	}
	else {
	    this.m.set(m);
	}

	// Flow depth
	if (fd < 0) {
	    error_log += "Flow depth must be greater than or equal to zero";
	    this.fd.unset();
	}
	else if (fd > cd) {
	    error_log += "Flow depth must be less than or equal to channel depth\n";
	    this.fd.unset();
	}
	else {
	    this.fd.set(fd);
	}

	// Update dependent vars 
	updateCalcs();
	printCalcs();
    }

    /** Update dependant variables if we have valid data */
    public void updateCalcs() {
	if (this.w1.isSet() &&
	    this.w2.isSet() &&
	    this.cd.isSet() &&
	    this.s.isSet() &&
	    this.m.isSet() &&
	    this.fd.isSet()) {
	    try {
		fw1 = calcFlowWidth();
		a = calcArea();
		p = calcWetPer();
		r = calcHydroRadius();
		v = calcVelocity();
		q = calcFlow();
	    }
	    catch(UnsetVariableException e) {
		System.out.println("Variables were unset!");
		System.exit(1);
	    }

	    calculated = true;
	}
	else {
	    calculated = false;
	}
    }
    
    /** Print dependant variables - updateCalcs() should be run first */
    public void printCalcs() {
	if (calculated) {
 	    System.out.println("Flow width: " + nf.format(fw1));
	    System.out.println("Cross sectional area of flow: " + nf.format(a));
	    System.out.println("Wetted perimeter: " + nf.format(p));
	    System.out.println("Hydrolic radius: " + nf.format(r));
	    System.out.println("Velocity: " + nf.format(v));
	    System.out.println("Flow: " + nf.format(q));
	}
	else {
	    System.out.println("Could make calculations because of invalid data:");
	    System.out.println(error_log);
	}
    }
    
    /* Formula functions - must be performed in order as listed */
    private double calcFlowWidth() throws UnsetVariableException {
	return  w2.get() + (fd.get() * (w1.get() - w2.get())) / cd.get();
    }

    private double calcArea() throws UnsetVariableException {
	return fd.get() * (w2.get() + (fw1 - w2.get()) / 2);
    }

    private double calcWetPer() throws UnsetVariableException {
	double temp = Math.pow(((fw1 - w2.get()) / 2), 2.0) + Math.pow(fd.get(), 2.0);
	return 2 * Math.sqrt(temp) + w2.get();
    }

    private double calcHydroRadius() {
	return p == 0 ? 0 : a / p; // avoid dividing by zero - define answer as zero when fd == 0
    }

    private double calcVelocity() throws UnsetVariableException {
	double r = calcHydroRadius();
	return (Math.pow(r, 2.0/3.0) * Math.pow(s.get(), 0.5)) / m.get();
    }

    private double calcFlow() {
	return a * v;
    }

    public double get_fw() {
	return fw1;
    }

    public boolean getCalculated() { return calculated; }
    public double getVelocity() { return v; }
    public double getFlowRate() { return q; }
    public boolean isSet_w1() { return w1.isSet(); }
    public boolean isSet_w2() { return w2.isSet(); }
    public boolean isSet_cd() { return cd.isSet(); }
    public boolean isSet_m() { return m.isSet(); }
    public boolean isSet_s() { return s.isSet(); }
    public boolean isSet_fd() { return fd.isSet(); }
}

// Parameter that can have no value - be unset
class SettableDouble {
    private boolean set;
    private double value;

    SettableDouble() {
	unset();
    }

    public boolean isSet() {
	return set;
    }

    public void set(double val) { 
	set = true;
	value = val; 
    }
    
    public void unset() {
	set = false;
    }

    public double get() throws UnsetVariableException {
	if (set == false) {
	    throw new UnsetVariableException();
	}
	else {
	    return value;
	}
    }
}

class UnsetVariableException extends Exception {
    UnsetVariableException() {}
    UnsetVariableException(String msg) { super(msg); }
}

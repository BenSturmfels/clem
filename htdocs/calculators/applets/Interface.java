import javax.swing.*;
import java.awt.*;
import java.awt.Graphics2D.*;
import java.awt.event.*;
import java.text.DecimalFormat;
import java.awt.geom.*;


public class Interface extends JApplet implements KeyListener {
    final Dimension diagramSize = new Dimension(200,200);
    final Color disabled = Color.lightGray;

    // Channel restrictions
    final double w1Def = 5;
    final double w2Def = 4;
    final double cdDef = 2;
    final double sDef = 0.2;
    final double mDef = 0.02;
    final double fdDef = cdDef / 2;

    final int defWidth = 3; // width of text fields

    private CheckedDoubleField w1Text;
    private CheckedDoubleField w2Text;
    private CheckedDoubleField cdText;
    private CheckedDoubleField sText;
    private CheckedDoubleField mText;
    private CheckedDoubleField fdText;

    JTextField vValue;
    JTextField qValue;
    Channel channel;
    Diagram dia;

    // Number formatting
    private static String format = "0.00";
    private static DecimalFormat nf = new DecimalFormat(format);
    
    public void init() {
	channel = new Channel();
	getContentPane().setBackground(Color.white);

	/* Set our applet layout */
	getContentPane().setLayout(new FlowLayout());
	
	/* Input box labels */
	JLabel w1Label = new JLabel("Top width:");
	w1Label.setHorizontalAlignment(JLabel.RIGHT);
	JLabel w1UnitsLabel = new JLabel("m");
	w1UnitsLabel.setHorizontalAlignment(JLabel.LEFT);

	System.out.println(w1Label.getFont());

	JLabel w2Label = new JLabel("Bottom width:");
	w2Label.setHorizontalAlignment(JLabel.RIGHT);
	JLabel w2UnitsLabel = new JLabel("m");
	w2UnitsLabel.setHorizontalAlignment(JLabel.LEFT);

	JLabel cdLabel = new JLabel("Channel depth:");
	cdLabel.setHorizontalAlignment(JLabel.RIGHT);
	JLabel cdUnitsLabel = new JLabel("m");
	cdUnitsLabel.setHorizontalAlignment(JLabel.LEFT);

	JLabel sLabel = new JLabel("Energy gradient:");
	sLabel.setHorizontalAlignment(JLabel.RIGHT);
	JLabel sUnitsLabel = new JLabel("m/m");
	sUnitsLabel.setHorizontalAlignment(JLabel.LEFT);

	JLabel mLabel = new JLabel("Roughness:");
	mLabel.setHorizontalAlignment(JLabel.RIGHT);
	JLabel mUnitsLabel = new JLabel("");
	mUnitsLabel.setHorizontalAlignment(JLabel.LEFT);

	JLabel fdLabel = new JLabel("Flow depth:");
	fdLabel.setHorizontalAlignment(JLabel.RIGHT);
	JLabel fdUnitsLabel = new JLabel("m");
	fdUnitsLabel.setHorizontalAlignment(JLabel.LEFT);

	/* Input boxes */
	w1Text = new CheckedDoubleField(w1Def, defWidth);
	w2Text = new CheckedDoubleField(w2Def, defWidth);
	cdText = new CheckedDoubleField(cdDef, defWidth);
	sText = new CheckedDoubleField(sDef, defWidth);
	mText = new CheckedDoubleField(mDef, defWidth);
	fdText = new CheckedDoubleField(fdDef, defWidth);

	/* Create container for inputs */
	JPanel numboxes = new JPanel();
	numboxes.setBackground(Color.white);
	numboxes.setLayout(new GridLayout(9,3,3,1));
	numboxes.setMaximumSize(new Dimension(5,5));

	/* Add input labels and boxes to labels container */
	numboxes.add(w1Label);
	numboxes.add(w1Text);
	numboxes.add(w1UnitsLabel);

	numboxes.add(w2Label);
	numboxes.add(w2Text);
	numboxes.add(w2UnitsLabel);
	
	numboxes.add(cdLabel);
	numboxes.add(cdText);
	numboxes.add(cdUnitsLabel);

	numboxes.add(sLabel);
	numboxes.add(sText);
	numboxes.add(sUnitsLabel);

	numboxes.add(mLabel);
	numboxes.add(mText);
	numboxes.add(mUnitsLabel);

	numboxes.add(fdLabel);
	numboxes.add(fdText);
	numboxes.add(fdUnitsLabel);

	/* Result boxes and labels */
	JLabel vLabel = new JLabel("Velocity:");
	vLabel.setHorizontalAlignment(JLabel.RIGHT);
	JLabel vUnitsLabel = new JLabel("m/sec");
	vUnitsLabel.setHorizontalAlignment(JLabel.LEFT);
	vValue = new JTextField("", defWidth);
	vValue.setBackground(disabled);
	vValue.setFocusable(false);

	JLabel qLabel = new JLabel("Unit discharge:");
	qLabel.setHorizontalAlignment(JLabel.RIGHT);
	JLabel qUnitsLabel = new JLabel("cubic m/s");
	qUnitsLabel.setHorizontalAlignment(JLabel.LEFT);
	qValue = new JTextField("", defWidth);
	qValue.setBackground(disabled);
	qValue.setFocusable(false);

	/* Add results to container */
	numboxes.add(vLabel);
	numboxes.add(vValue);
	numboxes.add(vUnitsLabel);
	numboxes.add(qLabel);
	numboxes.add(qValue);
	numboxes.add(qUnitsLabel);

	/* Add numboxes to main applet */
	getContentPane().add(numboxes);

	w1Text.addKeyListener(this);
	w2Text.addKeyListener(this);
	cdText.addKeyListener(this);
	sText.addKeyListener(this);
	mText.addKeyListener(this);
	fdText.addKeyListener(this);

	/* Graphic */
	dia = new Diagram();
	dia.setPreferredSize(diagramSize);
	getContentPane().add(dia);

	/* Do initial calculations */
	updateCalcs();
    }

    public void keyReleased(KeyEvent ke) {
	updateCalcs();
    }

    public void keyPressed(KeyEvent ke) {}
    public void keyTyped(KeyEvent ke) {}

    private void updateCalcs() {
	// Is the text format valid?
	if (w1Text.checkFormat() && w2Text.checkFormat() &&
	    cdText.checkFormat() && sText.checkFormat() &&
	    mText.checkFormat() && fdText.checkFormat()) {

	    // Plug the data into channel model
	    channel.setParameters(w1Text.getValue(), w2Text.getValue(), 
				  cdText.getValue(), sText.getValue(), 
				  mText.getValue(), fdText.getValue());
	    
	    // Check the validity of individual items and mark invalid
	    // if necessary.
	    if (!channel.isSet_w1()) w1Text.invalid();
	    if (!channel.isSet_w2()) w2Text.invalid();
	    if (!channel.isSet_cd()) cdText.invalid();
	    if (!channel.isSet_s()) sText.invalid();
	    if (!channel.isSet_m()) mText.invalid();
	    if (!channel.isSet_fd()) fdText.invalid();
	    
	    // Have the results been calculated?
	    if (channel.getCalculated()) {
		vValue.setText(nf.format(channel.getVelocity()));
		qValue.setText(nf.format(channel.getFlowRate()));
		dia.update(w1Text.getValue(), w2Text.getValue(), 
			   cdText.getValue(), fdText.getValue(), 
			   channel.get_fw());
	    }
	    else {
		vValue.setText("-");
		qValue.setText("-");
	    }
	}
	else {
	    vValue.setText("-");
	    qValue.setText("-");
	}	
    }
}

class CheckedDoubleField extends JTextField {
    final static Color error = Color.red;
    final static Color bg = Color.white;
    
    /* Regexp for floating point no. */
    private static final String floatPattern = 
	"\\d+|\\d+\\.|\\.\\d+|\\d+\\.\\d+"; 
    
    private double value;
    
    CheckedDoubleField(double def, int cols) {
	super(String.valueOf(def), cols );
    }

    public boolean checkFormat() {
	String text = getText();
	if(text.matches(floatPattern)) { // is it a fp number?
	    value = Double.valueOf(text).doubleValue();
	    valid();
	    return true;
	}
	else {
	    invalid();
	    return false;
	}
    }

    public double getValue() {
	return value;
    }

    public void invalid() {
	setBackground(error);
    }

    public void valid() {
	setBackground(bg);
    }
}

class Diagram extends JPanel {
    float w1;
    float cd;
    float w2;
    float fd;
    float fw;

    final static Color waterColour = Color.blue;
    final static Color channelColour = Color.black;
    final static Color bg = Color.white;

    GeneralPath channel;
    GeneralPath water;
   
    public Diagram() {
	setBackground(bg);
    }

    public void paintComponent(Graphics g) {
	super.paintComponent(g);
	Graphics2D g2d = (Graphics2D)g;
	g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING,RenderingHints.VALUE_ANTIALIAS_ON);
	g2d.setRenderingHint(RenderingHints.KEY_TEXT_ANTIALIASING,RenderingHints.VALUE_TEXT_ANTIALIAS_ON);

	Dimension panelSize = getSize();
	int maxsize = panelSize.width - 2;
	
	double scalefactor;
	double transX = 0;
	double transY = 0;

	// find largest of w1, w2 and cd
	if(w1 >= w2 && w1 >= cd) {
	    scalefactor = maxsize / w1;
	}
	else if(w2 >= w1 && w2 >= cd) {
	    scalefactor = maxsize / w2;
	    // translate by 1/2 of difference between top and bottom
	    transX = (w2 - w1)/2;
	}
	else { 
	    scalefactor = maxsize / cd;
	    if(w1 < w2) transX = (w2 - w1)/2;
	}

	// get scaling factor by dividing panel size by largest value
	// apply transform with this factor

	AffineTransform transform = AffineTransform.getScaleInstance(scalefactor, scalefactor);
	transform.translate(transX, transY);

	// Draw channel
	channel = new GeneralPath();
	channel.moveTo(0,0);
	channel.lineTo((float)((w1/2.0)-(w2/2.0)), cd); // down left side
	channel.lineTo((float)((w1/2.0)+(w2/2.0)), cd); // across bottom
	channel.lineTo(w1,0); // up right side
	channel.transform(transform);
	g2d.setPaint(channelColour);
	g2d.draw(channel);

	// Draw water
	water = new GeneralPath();
	water.moveTo((float)((w1/2.0)-(fw/2.0)), cd - fd);
	water.lineTo((float)((w1/2.0)+(fw/2.0)), cd - fd);
	water.lineTo((float)((w1/2.0)+(w2/2.0)), cd); // down left side
	water.lineTo((float)((w1/2.0)-(w2/2.0)), cd); // across bottom
	water.closePath();
	water.transform(transform);
	g2d.setPaint(waterColour);
	g2d.fill(water);
    }

    public void update(double w1, double w2, double cd, double fd, double fw) {
	this.w1 = (float) w1;
	this.w2 = (float) w2;
	this.cd = (float) cd;
	this.fd = (float) fd;
	this.fw = (float) fw;
	repaint();
    }
}

<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<title>The Soil &amp; Land Portal</title>
<link rel="stylesheet" href="../simple.css" type="text/css" />
<style type="text/css" media="all">@import "../complex.css";</style>
<link rel="stylesheet" href="../print.css" type="text/css" media="print" />

<script type="text/javascript" src="../scripts/menu.js"></script>
<style>
label {
  display: inline-block;
  min-width: 7.5em;
  text-align: right;
}
input[type="number"] {
  width: 4em;
}
</style>
<script src="channel.js"></script>
</head>
<body>
<div class="hide"><a href="#content" title="Skip navigation." accesskey="2">Skip navigation</a>.</div>

<div id="header">
<?php
$toroot = "..";
require("$toroot/scripts/header.php");
?>
</div>

<div id="wrapper">
<div id="nav">
<?php require("$toroot/scripts/menu.php"); ?>
</div>

<div id="content">
<h1>Channel flow calculator</h1>
<p>This simple program calculates the flow of water in a rectangular or trapezoidal channel using Manning's formula: <img class="eqn" src="eqns/mannings.gif" width="156" height="46" alt="V=(R^(2/3)S^(1/2))/eta" />The formula is only applicable to channels that have a relatively uniform profile and slope.</p>
<h2>Method</h2>
<ol>
<li>Choose a straight reach of channel with a length at least 5 times the width, with constant slope.</li>
<li>Survey at least 3 cross sections to determine average channel dimensions.</li>
<li>Survey the channel bed (as an estimation of the water surface level) to determine the energy gradient (slope) of the reach.
<li>Estimate a value for Manning's roughness coefficient <img style="display: inline" src="eqns/eta.gif" width="13" height="16" alt="eta" /></li>
<li>Enter channel dimensions into calculator to determine <strong>velocity</strong> and <strong>unit discharge</strong>.</li>
</ol>
<form name="mannings" style="width: 400px; float: left;">
<p>
<label for="w1">Top width:</label> <input type="number" name="w1" value="5" min="0" step="1"> m<br>
<p><label for="w2">Bottom width:</label> <input type="number" name="w2" value="4" min="0" step="1"> m</p>
<p><label for="cd">Channel depth:</label> <input type="number" name="cd" value="2" min="0" step="1"> m</p>
<p><label for="s">Energy gradient:</label> <input type="range" name="s" value="0.2" min="0" max="1" step="0.1"> <output name="s_output" for="s"></output> m/m</p>
<p><label for="m">Roughness:</label> <input type="range" name="m" value="0.02" min="0.01" max="0.10" step="0.01"> <output name="m_output" for="m"></output></p>
<p><label for="fd">Flow depth:</label> <input type="number" name="fd" value="1" min="0" step="1"> m</p>
<p><label for="v">Velocity:</label> <strong><output name="v" for="w1 w2 cd s m fd"></output></strong> m/sec</p>
<p><label for="q">Unit discharge:</label> <strong><output name="q" for="w1 w2 cd s m fd v"></output></strong> cubic m/s</p>
</form>

<canvas id="square" width="300" height="300"></canvas>
</div>
</div>

<div id="footer">
<?php require "$toroot/scripts/footer.php"; ?>
</div>

</body>
</html>
